# asciiartist

Simple ASCII-art generator.                                                   

## Example
```sh
./asciiartist.py -i tux.png --scale 0.2 --characters ' tuxTUX.'
```

```sh
                                                   
                      .xXUUUuu                     
                    x XXUUUUUUUX                   
                   XXXXUUUUUUUUUU                  
                  .XXUUUUUUUUUUUUt                 
                  XUXUUUUUXTTXUUUX                 
                  X uuXUUU.uuXXUUX                 
                  XuUUXUX XUXXUUUX                 
                  XX       XXXUUUX                 
                  x.     xxXXXXUUU                 
                  XXXXXXXXXXUUUXXUU                
                 XUX  XXXXXXXXXUUUUU.              
                xUU      XXXXX UUUUUUx             
               xUU              UUUUUUX            
              XUUt               UUUUUUX           
             XUU                  UUUUUUU.         
            XUU                    UXUUUUU.        
           XUU                     UXXUUUUX.       
          XXXU                      XXXUUUUX       
         .XXXx                      XXUUUUUX       
         XUUXX                    xxXUUUUUUX       
         XXXUXx                   XXXXUUUUUX       
            XXUXx                XXXUUUUUUUUx      
             XUUUUx            xXXX XXUUUUXXX      
              XXUUUUx.       xxXXXx  XXXXXXXXX     
               XXXUUXXXxxxxXXXXXXUx  xxXXXXXXXXX   
   x  xxxXxxxxxXXXXUUXXXXXXXXUUUUUXxxXXXXXXXXXXXX  
   XxxxXXXXXXXXXXXXUUUUUUUUUUUUUXXXxXXXXXXXXXUt    
   XXXXXXXXXXXXXXXXXXUUUUUUUUUUXtXxxXXXXXXUX       
        tXXXXXXXXXXU             XXXXXXXU          
              XXXX                XXXXX            
```

## Example (with pixel characters)
```sh
# Enable 'pixels' characters in the script first.
python3 asciiartist.py -i tux.png --scale 0.2
```

```sh
                      ▕▒▒▓▇▆▅▃▁                    
                    ░░▒▒███████▙                   
                   ▒▒▒▓█████████▓                  
                  ▕▒▒▓███████████▋                 
                  ▒▒▀▜████▀▀▀▜███▊                 
                  ▐▏▗▒▜██▍▕▅▖▒███▊                 
                  ▐▖▜▓▐▀▜▏▓█▌▒███▊                 
                  ▐▛   ░░░░▒▒▓███▉                 
                  ▐░  ▕░░░▒▒▒▓████▖                
                  ▓▉▒▒▒▒▒▓▓▓▓██▒▓██▖               
                 ▐█▋ ▕▒▒▒▒▒▒▒▒▜▓▓██▓▖              
                ▗██▏    ░░▒▒▒▒▕██████▃             
               ▐██▘            ▕██████▙            
              ▟██▘              ▝██████▓▖          
             ▟██▘                ▝▓█████▓▖         
            ▟█▛░                  ▝▓▓████▓▖        
           ▟██░                   ░▝▒▒████▓▖       
          ▒█▓▒░                  ░░░▓▒▓████▒       
         ▕▓▓▒░░                  ░░░▐▓█████▓▏      
         ▒██▒▒░                  ░░▒▐██████▒▏      
         ░▒▒▓▒▒░                ░░▒▒▒▓█████▒       
        ▕░░░▒▜█▒▒░             ░░▒▒▒▓██████▒░      
       ░░░░░░▒▜██▙▒░          ░░▒▒▒░▒▜████▓▒▒      
  ▕░░░░░░░░░░░▒▒███▙▒░░   ░░░░▒▒▒▒░░░░▒▒▒▒▒▒▒▒     
  ▕░░░░░░░░░░░░▒▒███▓▒▒▒▒▒▒▒▒▒▒▒█▎░░░░▒▒▒▒▒▒▒▒▒▒   
   ░░░░░▒▒▒▒░░░░▒▒███▓▒▒▒▒▒▒▒▟███▒▒░░▒▒▒▒▒▒▒▒▒▒▒░  
  ▕▒░░░▒▒▒▒▒▒▒▒▒▒▒▒▓████████████▉▒▒░▒▒▒▒▒▒▒▒▒▒▘    
  ▕▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▛▓███████▓▛▀▒▒░░▒▒▒▒▒▒▒▀▔      
      ▔▔▝▒▒▒▒▒▒▒▒▒▒▒    ▔▔▔     ▕▒▒▒▒▒▒▒▒▔         
            ▔▔▀▒▒▀▔              ▕▒▒▒▒▀▔           
                                                   
```


